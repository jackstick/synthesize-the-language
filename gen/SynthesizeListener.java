// Generated from C:/Users/Taavi/IdeaProjects/Synthesize/src\Synthesize.g4 by ANTLR 4.x
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SynthesizeParser}.
 */
public interface SynthesizeListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SynthesizeParser#akord}.
	 * @param ctx the parse tree
	 */
	void enterAkord(@NotNull SynthesizeParser.AkordContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthesizeParser#akord}.
	 * @param ctx the parse tree
	 */
	void exitAkord(@NotNull SynthesizeParser.AkordContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthesizeParser#noot}.
	 * @param ctx the parse tree
	 */
	void enterNoot(@NotNull SynthesizeParser.NootContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthesizeParser#noot}.
	 * @param ctx the parse tree
	 */
	void exitNoot(@NotNull SynthesizeParser.NootContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthesizeParser#noodinimi}.
	 * @param ctx the parse tree
	 */
	void enterNoodinimi(@NotNull SynthesizeParser.NoodinimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthesizeParser#noodinimi}.
	 * @param ctx the parse tree
	 */
	void exitNoodinimi(@NotNull SynthesizeParser.NoodinimiContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthesizeParser#paus}.
	 * @param ctx the parse tree
	 */
	void enterPaus(@NotNull SynthesizeParser.PausContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthesizeParser#paus}.
	 * @param ctx the parse tree
	 */
	void exitPaus(@NotNull SynthesizeParser.PausContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthesizeParser#pilliblokk}.
	 * @param ctx the parse tree
	 */
	void enterPilliblokk(@NotNull SynthesizeParser.PilliblokkContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthesizeParser#pilliblokk}.
	 * @param ctx the parse tree
	 */
	void exitPilliblokk(@NotNull SynthesizeParser.PilliblokkContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthesizeParser#volume}.
	 * @param ctx the parse tree
	 */
	void enterVolume(@NotNull SynthesizeParser.VolumeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthesizeParser#volume}.
	 * @param ctx the parse tree
	 */
	void exitVolume(@NotNull SynthesizeParser.VolumeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthesizeParser#pillinimi}.
	 * @param ctx the parse tree
	 */
	void enterPillinimi(@NotNull SynthesizeParser.PillinimiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthesizeParser#pillinimi}.
	 * @param ctx the parse tree
	 */
	void exitPillinimi(@NotNull SynthesizeParser.PillinimiContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthesizeParser#muutuja}.
	 * @param ctx the parse tree
	 */
	void enterMuutuja(@NotNull SynthesizeParser.MuutujaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthesizeParser#muutuja}.
	 * @param ctx the parse tree
	 */
	void exitMuutuja(@NotNull SynthesizeParser.MuutujaContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthesizeParser#pillideklareerimine}.
	 * @param ctx the parse tree
	 */
	void enterPillideklareerimine(@NotNull SynthesizeParser.PillideklareerimineContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthesizeParser#pillideklareerimine}.
	 * @param ctx the parse tree
	 */
	void exitPillideklareerimine(@NotNull SynthesizeParser.PillideklareerimineContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthesizeParser#synthesize}.
	 * @param ctx the parse tree
	 */
	void enterSynthesize(@NotNull SynthesizeParser.SynthesizeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthesizeParser#synthesize}.
	 * @param ctx the parse tree
	 */
	void exitSynthesize(@NotNull SynthesizeParser.SynthesizeContext ctx);
}