// Generated from C:/Users/Taavi/IdeaProjects/Synthesize/src\Synthesize.g4 by ANTLR 4.x
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SynthesizeParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__26=1, T__25=2, T__24=3, T__23=4, T__22=5, T__21=6, T__20=7, T__19=8, 
		T__18=9, T__17=10, T__16=11, T__15=12, T__14=13, T__13=14, T__12=15, T__11=16, 
		T__10=17, T__9=18, T__8=19, T__7=20, T__6=21, T__5=22, T__4=23, T__3=24, 
		T__2=25, T__1=26, T__0=27, Lowercase=28, Uppercase=29, Number=30, Kommentaar=31, 
		Whitespace=32;
	public static final String[] tokenNames = {
		"<INVALID>", "'guitar5distortion'", "'base2picked'", "'&'", "','", "':'", 
		"'guitar4overdrive'", "'volume'", "'piano'", "'churchbell'", "'violin'", 
		"'harp'", "'base1acoustic'", "'guitar2steel'", "'.'", "'_'", "'trombone'", 
		"'pause'", "'trumpet'", "'='", "'accordion'", "'guitar3clean'", "'begin'", 
		"'new'", "'guitar1nylon'", "'#'", "'contrabass'", "'end'", "Lowercase", 
		"Uppercase", "Number", "Kommentaar", "Whitespace"
	};
	public static final int
		RULE_synthesize = 0, RULE_noot = 1, RULE_paus = 2, RULE_akord = 3, RULE_noodinimi = 4, 
		RULE_pillinimi = 5, RULE_pillideklareerimine = 6, RULE_muutuja = 7, RULE_pilliblokk = 8, 
		RULE_volume = 9;
	public static final String[] ruleNames = {
		"synthesize", "noot", "paus", "akord", "noodinimi", "pillinimi", "pillideklareerimine", 
		"muutuja", "pilliblokk", "volume"
	};

	@Override
	public String getGrammarFileName() { return "Synthesize.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SynthesizeParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class SynthesizeContext extends ParserRuleContext {
		public PillideklareerimineContext pillideklareerimine() {
			return getRuleContext(PillideklareerimineContext.class,0);
		}
		public VolumeContext volume() {
			return getRuleContext(VolumeContext.class,0);
		}
		public SynthesizeContext synthesize(int i) {
			return getRuleContext(SynthesizeContext.class,i);
		}
		public PilliblokkContext pilliblokk() {
			return getRuleContext(PilliblokkContext.class,0);
		}
		public List<SynthesizeContext> synthesize() {
			return getRuleContexts(SynthesizeContext.class);
		}
		public SynthesizeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_synthesize; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).enterSynthesize(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).exitSynthesize(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthesizeVisitor ) return ((SynthesizeVisitor<? extends T>)visitor).visitSynthesize(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SynthesizeContext synthesize() throws RecognitionException {
		return synthesize(0);
	}

	private SynthesizeContext synthesize(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		SynthesizeContext _localctx = new SynthesizeContext(_ctx, _parentState);
		SynthesizeContext _prevctx = _localctx;
		int _startState = 0;
		enterRecursionRule(_localctx, 0, RULE_synthesize, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(24);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				{
				setState(21); pilliblokk();
				}
				break;
			case 2:
				{
				setState(22); volume();
				}
				break;
			case 3:
				{
				setState(23); pillideklareerimine();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(34);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new SynthesizeContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_synthesize);
					setState(26);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(28); 
					_errHandler.sync(this);
					_alt = 1;
					do {
						switch (_alt) {
						case 1:
							{
							{
							setState(27); synthesize(0);
							}
							}
							break;
						default:
							throw new NoViableAltException(this);
						}
						setState(30); 
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
					} while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER );
					}
					} 
				}
				setState(36);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class NootContext extends ParserRuleContext {
		public List<TerminalNode> Number() { return getTokens(SynthesizeParser.Number); }
		public NoodinimiContext noodinimi() {
			return getRuleContext(NoodinimiContext.class,0);
		}
		public TerminalNode Number(int i) {
			return getToken(SynthesizeParser.Number, i);
		}
		public NootContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_noot; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).enterNoot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).exitNoot(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthesizeVisitor ) return ((SynthesizeVisitor<? extends T>)visitor).visitNoot(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NootContext noot() throws RecognitionException {
		NootContext _localctx = new NootContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_noot);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(37); match(Number);
			setState(38); noodinimi();
			setState(39); match(Number);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PausContext extends ParserRuleContext {
		public TerminalNode Number() { return getToken(SynthesizeParser.Number, 0); }
		public PausContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_paus; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).enterPaus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).exitPaus(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthesizeVisitor ) return ((SynthesizeVisitor<? extends T>)visitor).visitPaus(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PausContext paus() throws RecognitionException {
		PausContext _localctx = new PausContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_paus);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(41); match(Number);
			setState(42); match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AkordContext extends ParserRuleContext {
		public NootContext noot(int i) {
			return getRuleContext(NootContext.class,i);
		}
		public List<NootContext> noot() {
			return getRuleContexts(NootContext.class);
		}
		public AkordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_akord; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).enterAkord(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).exitAkord(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthesizeVisitor ) return ((SynthesizeVisitor<? extends T>)visitor).visitAkord(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AkordContext akord() throws RecognitionException {
		AkordContext _localctx = new AkordContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_akord);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(44); noot();
			setState(47); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(45); match(T__23);
				setState(46); noot();
				}
				}
				setState(49); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__23 );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NoodinimiContext extends ParserRuleContext {
		public TerminalNode Lowercase() { return getToken(SynthesizeParser.Lowercase, 0); }
		public NoodinimiContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_noodinimi; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).enterNoodinimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).exitNoodinimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthesizeVisitor ) return ((SynthesizeVisitor<? extends T>)visitor).visitNoodinimi(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NoodinimiContext noodinimi() throws RecognitionException {
		NoodinimiContext _localctx = new NoodinimiContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_noodinimi);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(51); match(Lowercase);
			setState(53);
			_la = _input.LA(1);
			if (_la==T__24) {
				{
				setState(52); match(T__24);
				}
			}

			setState(56);
			_la = _input.LA(1);
			if (_la==T__2) {
				{
				setState(55); match(T__2);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PillinimiContext extends ParserRuleContext {
		public PillinimiContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pillinimi; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).enterPillinimi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).exitPillinimi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthesizeVisitor ) return ((SynthesizeVisitor<? extends T>)visitor).visitPillinimi(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PillinimiContext pillinimi() throws RecognitionException {
		PillinimiContext _localctx = new PillinimiContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_pillinimi);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(58);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__26) | (1L << T__25) | (1L << T__21) | (1L << T__19) | (1L << T__18) | (1L << T__17) | (1L << T__16) | (1L << T__15) | (1L << T__14) | (1L << T__11) | (1L << T__9) | (1L << T__7) | (1L << T__6) | (1L << T__3) | (1L << T__1))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PillideklareerimineContext extends ParserRuleContext {
		public MuutujaContext muutuja() {
			return getRuleContext(MuutujaContext.class,0);
		}
		public PillinimiContext pillinimi() {
			return getRuleContext(PillinimiContext.class,0);
		}
		public PillideklareerimineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pillideklareerimine; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).enterPillideklareerimine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).exitPillideklareerimine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthesizeVisitor ) return ((SynthesizeVisitor<? extends T>)visitor).visitPillideklareerimine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PillideklareerimineContext pillideklareerimine() throws RecognitionException {
		PillideklareerimineContext _localctx = new PillideklareerimineContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_pillideklareerimine);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(60); muutuja();
			setState(61); match(T__8);
			setState(62); match(T__4);
			setState(63); pillinimi();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MuutujaContext extends ParserRuleContext {
		public List<TerminalNode> Uppercase() { return getTokens(SynthesizeParser.Uppercase); }
		public TerminalNode Uppercase(int i) {
			return getToken(SynthesizeParser.Uppercase, i);
		}
		public List<TerminalNode> Lowercase() { return getTokens(SynthesizeParser.Lowercase); }
		public List<TerminalNode> Number() { return getTokens(SynthesizeParser.Number); }
		public TerminalNode Number(int i) {
			return getToken(SynthesizeParser.Number, i);
		}
		public TerminalNode Lowercase(int i) {
			return getToken(SynthesizeParser.Lowercase, i);
		}
		public MuutujaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_muutuja; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).enterMuutuja(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).exitMuutuja(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthesizeVisitor ) return ((SynthesizeVisitor<? extends T>)visitor).visitMuutuja(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MuutujaContext muutuja() throws RecognitionException {
		MuutujaContext _localctx = new MuutujaContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_muutuja);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(65);
			_la = _input.LA(1);
			if ( !(_la==Lowercase || _la==Uppercase) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			setState(67); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(66);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__12) | (1L << Lowercase) | (1L << Uppercase) | (1L << Number))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				}
				setState(69); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__12) | (1L << Lowercase) | (1L << Uppercase) | (1L << Number))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PilliblokkContext extends ParserRuleContext {
		public List<AkordContext> akord() {
			return getRuleContexts(AkordContext.class);
		}
		public PausContext paus(int i) {
			return getRuleContext(PausContext.class,i);
		}
		public List<PausContext> paus() {
			return getRuleContexts(PausContext.class);
		}
		public MuutujaContext muutuja() {
			return getRuleContext(MuutujaContext.class,0);
		}
		public NootContext noot(int i) {
			return getRuleContext(NootContext.class,i);
		}
		public List<NootContext> noot() {
			return getRuleContexts(NootContext.class);
		}
		public AkordContext akord(int i) {
			return getRuleContext(AkordContext.class,i);
		}
		public PilliblokkContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pilliblokk; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).enterPilliblokk(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).exitPilliblokk(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthesizeVisitor ) return ((SynthesizeVisitor<? extends T>)visitor).visitPilliblokk(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PilliblokkContext pilliblokk() throws RecognitionException {
		PilliblokkContext _localctx = new PilliblokkContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_pilliblokk);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71); muutuja();
			setState(72); match(T__13);
			setState(73); match(T__5);
			setState(74); match(T__22);
			setState(80);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Number) {
				{
				setState(78);
				switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
				case 1:
					{
					setState(75); noot();
					}
					break;
				case 2:
					{
					setState(76); akord();
					}
					break;
				case 3:
					{
					setState(77); paus();
					}
					break;
				}
				}
				setState(82);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(83); match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VolumeContext extends ParserRuleContext {
		public TerminalNode Number() { return getToken(SynthesizeParser.Number, 0); }
		public VolumeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_volume; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).enterVolume(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthesizeListener ) ((SynthesizeListener)listener).exitVolume(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthesizeVisitor ) return ((SynthesizeVisitor<? extends T>)visitor).visitVolume(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VolumeContext volume() throws RecognitionException {
		VolumeContext _localctx = new VolumeContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_volume);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(85); match(T__20);
			setState(86); match(T__8);
			setState(87); match(Number);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 0: return synthesize_sempred((SynthesizeContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean synthesize_sempred(SynthesizeContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\"\\\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\3"+
		"\2\3\2\3\2\3\2\5\2\33\n\2\3\2\3\2\6\2\37\n\2\r\2\16\2 \7\2#\n\2\f\2\16"+
		"\2&\13\2\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\5\3\5\3\5\6\5\62\n\5\r\5\16\5\63"+
		"\3\6\3\6\5\68\n\6\3\6\5\6;\n\6\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\6\t"+
		"F\n\t\r\t\16\tG\3\n\3\n\3\n\3\n\3\n\3\n\3\n\7\nQ\n\n\f\n\16\nT\13\n\3"+
		"\n\3\n\3\13\3\13\3\13\3\13\3\13\2\3\2\f\2\4\6\b\n\f\16\20\22\24\2\5\n"+
		"\2\3\4\b\b\n\17\22\22\24\24\26\27\32\32\34\34\3\2\36\37\4\2\21\21\36 "+
		"\\\2\32\3\2\2\2\4\'\3\2\2\2\6+\3\2\2\2\b.\3\2\2\2\n\65\3\2\2\2\f<\3\2"+
		"\2\2\16>\3\2\2\2\20C\3\2\2\2\22I\3\2\2\2\24W\3\2\2\2\26\27\b\2\1\2\27"+
		"\33\5\22\n\2\30\33\5\24\13\2\31\33\5\16\b\2\32\26\3\2\2\2\32\30\3\2\2"+
		"\2\32\31\3\2\2\2\33$\3\2\2\2\34\36\f\3\2\2\35\37\5\2\2\2\36\35\3\2\2\2"+
		"\37 \3\2\2\2 \36\3\2\2\2 !\3\2\2\2!#\3\2\2\2\"\34\3\2\2\2#&\3\2\2\2$\""+
		"\3\2\2\2$%\3\2\2\2%\3\3\2\2\2&$\3\2\2\2\'(\7 \2\2()\5\n\6\2)*\7 \2\2*"+
		"\5\3\2\2\2+,\7 \2\2,-\7\23\2\2-\7\3\2\2\2.\61\5\4\3\2/\60\7\6\2\2\60\62"+
		"\5\4\3\2\61/\3\2\2\2\62\63\3\2\2\2\63\61\3\2\2\2\63\64\3\2\2\2\64\t\3"+
		"\2\2\2\65\67\7\36\2\2\668\7\5\2\2\67\66\3\2\2\2\678\3\2\2\28:\3\2\2\2"+
		"9;\7\33\2\2:9\3\2\2\2:;\3\2\2\2;\13\3\2\2\2<=\t\2\2\2=\r\3\2\2\2>?\5\20"+
		"\t\2?@\7\25\2\2@A\7\31\2\2AB\5\f\7\2B\17\3\2\2\2CE\t\3\2\2DF\t\4\2\2E"+
		"D\3\2\2\2FG\3\2\2\2GE\3\2\2\2GH\3\2\2\2H\21\3\2\2\2IJ\5\20\t\2JK\7\20"+
		"\2\2KL\7\30\2\2LR\7\7\2\2MQ\5\4\3\2NQ\5\b\5\2OQ\5\6\4\2PM\3\2\2\2PN\3"+
		"\2\2\2PO\3\2\2\2QT\3\2\2\2RP\3\2\2\2RS\3\2\2\2SU\3\2\2\2TR\3\2\2\2UV\7"+
		"\35\2\2V\23\3\2\2\2WX\7\t\2\2XY\7\25\2\2YZ\7 \2\2Z\25\3\2\2\2\13\32 $"+
		"\63\67:GPR";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}