// Generated from C:/Users/Taavi/IdeaProjects/Synthesize/src\Synthesize.g4 by ANTLR 4.x
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SynthesizeParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SynthesizeVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SynthesizeParser#akord}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAkord(@NotNull SynthesizeParser.AkordContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthesizeParser#noot}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoot(@NotNull SynthesizeParser.NootContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthesizeParser#noodinimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoodinimi(@NotNull SynthesizeParser.NoodinimiContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthesizeParser#paus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPaus(@NotNull SynthesizeParser.PausContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthesizeParser#pilliblokk}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPilliblokk(@NotNull SynthesizeParser.PilliblokkContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthesizeParser#volume}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVolume(@NotNull SynthesizeParser.VolumeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthesizeParser#pillinimi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPillinimi(@NotNull SynthesizeParser.PillinimiContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthesizeParser#muutuja}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMuutuja(@NotNull SynthesizeParser.MuutujaContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthesizeParser#pillideklareerimine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPillideklareerimine(@NotNull SynthesizeParser.PillideklareerimineContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthesizeParser#synthesize}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSynthesize(@NotNull SynthesizeParser.SynthesizeContext ctx);
}