grammar Synthesize;

synthesize
    :   pilliblokk
    |   volume
    |   pillideklareerimine
    |   synthesize(synthesize)+
    ;

noot
    :   Number noodinimi Number
    ;

paus
    :   Number 'pause'
    ;

akord
    :   noot (','noot)+
    ;

noodinimi
    :   Lowercase('&')?('#')?
    ;

pillinimi
    :   ('piano'|'churchbell'
    |   'accordion'|'violin'
    |   'contrabass'|'guitar1nylon'
    |   'guitar2steel'|'guitar3clean'
    |   'guitar4overdrive'|'guitar5distortion'
    |   'base1acoustic'|'base2picked'
    |   'harp'|'trumpet'
    |   'trombone'|'churchbell')
    ;

pillideklareerimine
    :   muutuja'=''new'pillinimi
    ;

muutuja
    :   (Uppercase|Lowercase)(Uppercase|Lowercase|Number|'_')+
    ;

pilliblokk
    :   muutuja '.' 'begin'':'(noot|akord|paus)*'end'
    ;

/*oktav
    :   ('1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'|'9'|'10')
    ;

noodipikkus
    :   ('1'|'2'|'4'|'8'|'16'|'32'|'64')
    ;*/

volume
    :   'volume' '=' Number
    ;

Lowercase
    :   [a-z]
    ;

Uppercase
    :   [A-Z]
    ;

Number
    :   [0-9]+
    ;

/*MuutujaAsi
    :   [a-zA-Z]+
    ;*/

/*Any
    :   [a-zA-Z0-9]+
    ;*/

Kommentaar
    :   '/*' .*? '*/' -> skip
    ;

Whitespace
    :   [ \r\t\n]+ -> skip
    ;
