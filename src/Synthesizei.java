import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import javax.sound.midi.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Synthesizei {

    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("Walking: " + args[0]);
        String inputFile = null;
        if (args.length > 0) inputFile = args[0];
        InputStream is = System.in;
        if (inputFile != null) is = new FileInputStream(inputFile);
        ANTLRInputStream input = new ANTLRInputStream(is);
        SynthesizeLexer lexer = new SynthesizeLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        SynthesizeParser parser = new SynthesizeParser(tokens);
        ParseTree tree = parser.synthesize(); // parse

        String parsitav = tree.toStringTree(parser);
        System.out.println(parsitav);

        String[] array1 = parsitav.split("[\\(\\)]");

        Map<String, Integer> midiNoodid = new HashMap<String, Integer>();
            midiNoodid.put("c", 0);
            midiNoodid.put("d", 2);
            midiNoodid.put("e", 4);
            midiNoodid.put("f", 5);
            midiNoodid.put("g", 7);
            midiNoodid.put("a", 9);
            midiNoodid.put("b", 11);
        Map<String, Integer> midiPillid = new HashMap<String, Integer>();
            midiPillid.put("piano",2);
            midiPillid.put("accordion",21);
            midiPillid.put("violin",40);
            midiPillid.put("contrabass",43);
            midiPillid.put("guitar1nylon",24);
            midiPillid.put("guitar2steel",25);
            midiPillid.put("guitar3clean",27);
            midiPillid.put("guitar4overdrive",29);
            midiPillid.put("guitar5distortion",30);
            midiPillid.put("base1acoustic",32);
            midiPillid.put("base2picked",34);
            midiPillid.put("harp",46);
            midiPillid.put("trumpet",56);
            midiPillid.put("trombone",57);
            midiPillid.put("churchbell",177);
            midiPillid.put("scream",150);



        Map<String, String> pillid = new HashMap<String, String>();
        Map<String, List<String>> noodid = new HashMap<String, List<String>>();
        List<String> pillinoodid = null;
        List<String> akord = new ArrayList<String>();

        int volume = 0;
        String muutujaNimi = "";
        String akordString = "";

        for (int i = 0; i < array1.length; i++) {
            System.out.println(array1[i]);
            if (array1[i].matches("^muutuja(.*)")) {
                String[] muutujaArray = array1[i].split(" ");
                String muutujanimi = "";
                for (int j = 0; j < muutujaArray.length; j++) {
                    if (j >= 1 && j < muutujaArray.length) {
                        muutujanimi += muutujaArray[j];
                    }
                }
                if (!pillid.containsKey(muutujanimi)) {
                    pillid.put(muutujanimi, array1[i + 2].split(" ")[1]);
                }
            } else if (array1[i].matches("^volume(.*)")) {
                String[] volumeArray = array1[i].split(" ");
                volume = Integer.parseInt(volumeArray[3]);

            } else if (array1[i].matches("^ . begin(.*)")) {
                pillinoodid = new ArrayList<String>();
                String[] muutujaArray = array1[i - 1].split(" ");
                String muutujanimi = "";
                for (int j = 0; j < muutujaArray.length; j++) {
                    if (j >= 1 && j < muutujaArray.length) {
                        muutujanimi += muutujaArray[j];
                    }
                }
                muutujaNimi = muutujanimi;
            } else if (array1[i].matches("^paus(.*)") || array1[i].matches("^noot(.*)") || array1[i].matches("^akord(.*)") || array1[i].matches("^ end(.*)") || array1[i].matches("^ ,(.*)")) {
                if (array1[i].matches("^paus(.*)")) {
                    String[] muutujaArray = array1[i].split(" ");
                    pillinoodid.add(muutujaArray[1]);
                } else if (array1[i].matches("^noot(.*)") && (!(array1[i - 1].matches("^akord(.*)"))) && !(array1[i - 1].matches("^ ,(.*)"))) {
                    String noodiPikkus = (array1[i].split(" ")[1]);
                    int noodiValue = 0;
                    if ((array1[i + 1].split(" ")).length == 3) {
                        if (array1[i + 1].split(" ")[2].equals("#")) {
                            noodiValue = (midiNoodid.get(array1[i + 1].split(" ")[1]) + 1) + (Integer.parseInt(array1[i + 2].split(" ")[1]) * 12);
                        } else if (array1[i + 1].split(" ")[2].equals("&")) {
                            noodiValue = (midiNoodid.get(array1[i + 1].split(" ")[1]) - 1) + (Integer.parseInt(array1[i + 2].split(" ")[1]) * 12);
                        }
                    } else if ((array1[i + 1].split(" ")).length == 2) {
                        noodiValue = (midiNoodid.get(array1[i + 1].split(" ")[1])) + (Integer.parseInt(array1[i + 2].split(" ")[1]) * 12);
                    }

                    String noodiNimi = noodiPikkus + "/" + noodiValue;
                    pillinoodid.add(noodiNimi);

                } else if (array1[i].matches("^akord(.*)") || array1[i].matches("^ ,(.*)")) {
                    String noodiPikkus = (array1[i + 1].split(" ")[1]);
                    int noodiValue = 0;
                    if ((array1[i + 2].split(" ")).length == 3) {
                        if (array1[i + 2].split(" ")[2].equals("#")) {
                            noodiValue = (midiNoodid.get(array1[i + 2].split(" ")[1]) + 1) + (Integer.parseInt(array1[i + 3].split(" ")[1]) * 12);
                        } else if (array1[i + 2].split(" ")[2].equals("&")) {
                            noodiValue = (midiNoodid.get(array1[i + 2].split(" ")[1]) - 1) + (Integer.parseInt(array1[i + 3].split(" ")[1]) * 12);
                        }
                    } else if ((array1[i + 2].split(" ")).length == 2) {
                        noodiValue = (midiNoodid.get(array1[i + 2].split(" ")[1])) + (Integer.parseInt(array1[i + 3].split(" ")[1]) * 12);
                    }
                    String noodiNimi = noodiPikkus + "/" + noodiValue;
                    akord.add(noodiNimi);
                    if (!(array1[i + 4].matches("^ ,(.*)"))) {
                        int wesostupid = 0;
                        for (String element : akord) {
                            if (wesostupid == 0) {
                                akordString += element;
                                wesostupid = 1;
                            } else {
                                akordString += ", ";
                                akordString += element;
                            }
                        }
                        pillinoodid.add(akordString);
                        akordString = "";
                        akord.clear();
                    }
                }
            }
            if (array1[i].matches("^ end$")) {
                noodid.put(muutujaNimi, pillinoodid);
            }
        }
        System.out.println("Volume: " + volume);
        System.out.println(pillid.entrySet());
        System.out.println(noodid.entrySet());
        //System.out.println(noodid.get("klaver1"));

        /*for (String element : noodid.get("klaver1")) {
            System.out.println(element);
        }*/


        //System.out.println(parsitav);
        //System.out.println(tree.toStringTree(parser));
        /*TestVisitor test = new TestVisitor();
        System.out.println(test.visit(tree));*/

        //int suvaline = 1;
        Map<String, List<String>> pillnoot = new HashMap<String, List<String>>();

        for(String element : pillid.keySet()){
            pillnoot.put(pillid.get(element), noodid.get(element));

        }

        Synthesizer synth = null;
        try {
            synth = MidiSystem.getSynthesizer();
            synth.open();

        } catch (MidiUnavailableException e1) {

            e1.printStackTrace();
        }
        MidiChannel channels[] = null;
        Soundbank bank;
        Instrument instrs[] = null;
        try {
            channels = synth.getChannels();
            bank = synth.getDefaultSoundbank();
            synth.loadAllInstruments(bank);
            instrs = synth.getLoadedInstruments();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        Instrument instrument1 = null;
        /*instrument1 = instrs[2];
        Patch instrument1Patch = instrument1.getPatch();
        channels[1].programChange(instrument1Patch.getBank(),instrument1Patch.getProgram());*/
        int suvaline = 0;
        for(String element : pillnoot.keySet()){
            suvaline+=1;
            instrument1 = instrs[midiPillid.get(element)];
            Patch instrument1Patch = instrument1.getPatch();
            channels[suvaline].programChange(instrument1Patch.getBank(), instrument1Patch.getProgram());
            ThreadClass lõim1 = new ThreadClass(pillnoot.get(element),volume, channels[suvaline]);
            Thread t = new Thread(lõim1, "Lõim-1");
            t.start();
            /*for(String element2 : pillnoot.get(element)){
                System.out.println(element2);
                if(element2.length()==1||element2.length()==2){
                    Thread.sleep(1600/Integer.parseInt(element2));
                }
                else if(element2.length()>=3&&element2.length()<=5){
                    channels[suvaline].noteOn(Integer.parseInt(element2.split("/")[1]), volume);
                    Thread.sleep(1600/Integer.parseInt(element2.split("/")[0]));
                    channels[suvaline].noteOff(Integer.parseInt(element2.split("/")[1]));
                }
                else{
                    for(int i=0;i<element2.split(", ").length;i++){
                        channels[suvaline].noteOn(Integer.parseInt(element2.split(", ")[i].split("/")[1]), volume);
                    }
                    Thread.sleep(1600/Integer.parseInt((element2.split(", ")[0].split("/"))[0]));
                    for(int i=0;i<element2.split(", ").length;i++){
                        channels[suvaline].noteOff(Integer.parseInt(element2.split(", ")[i].split("/")[1]));
                    }

                }
            }*/
        }


        //for(List<String> element : noodid.keySet()){
            //System.out.println(element);
        //}
    }
}
