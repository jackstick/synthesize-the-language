import javax.sound.midi.MidiChannel;
import java.util.List;
/**
 * Created by Taavi on 28.05.2014.
 */
public class ThreadClass implements Runnable {
   //String pillinimi;
    List<String> noodid;
    int volume;
    MidiChannel channel;

    public ThreadClass(List<String> noodid, int volume, MidiChannel channel) {
        //this.pillinimi = pillinimi;
        this.noodid = noodid;
        this.volume = volume;
        this.channel = channel;
    }

    public void run() {
        for (String element2 : noodid) {
            System.out.println(element2);
            if (element2.length() == 1 || element2.length() == 2) {
                try {
                    Thread.sleep(1600 / Integer.parseInt(element2));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else if (element2.length() >= 3 && element2.length() <= 5) {
                channel.noteOn(Integer.parseInt(element2.split("/")[1]), volume);
                try {
                    Thread.sleep(1600 / Integer.parseInt(element2.split("/")[0]));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                channel.noteOff(Integer.parseInt(element2.split("/")[1]));
            } else {
                for (int i = 0; i < element2.split(", ").length; i++) {
                    channel.noteOn(Integer.parseInt(element2.split(", ")[i].split("/")[1]), volume);
                }
                try {
                    Thread.sleep(1600 / Integer.parseInt((element2.split(", ")[0].split("/"))[0]));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < element2.split(", ").length; i++) {
                    channel.noteOff(Integer.parseInt(element2.split(", ")[i].split("/")[1]));
                }

            }

        }
    }
}
